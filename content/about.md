+++
title = 'About me'
date = 2023-09-27T13:41:17-03:00
draft = false
+++

Iniciou os estudos na área musical com 8 anos, com enfoque para violino, estudando posteriormente outros instrumentos como viola clássica, flauta doce, flauta transversal, assim obtendo experiências musicais diversas, aos 15 anos mudando de área de estudo para a área de metalmecanica e posteriormente aos 21 iniciando curso superior em Engenharia Mecatrônica, atualmente os estudos musicais são uma atividade de lazer.