+++
title = 'DAddario'
date = 2023-10-03T11:41:50-03:00
draft = false
+++
Empresa mais recente com a fundação por volta de 1970 por um Engenheiro e músico norte americano que inovou na produção de encordoamentos feitos em máquinas de precisão, resultando em cordas com maior repetibilidade, trabalhando com núcleos de aço e alguns sinteticos atualmente é um fabricante com renome de mercado pela excelência dos produtos.