+++
title = 'Thomastik'
date = 2023-10-03T11:42:16-03:00
draft = false
+++

A empresa [Thomastik Infeld Vienna](https://www.thomastik-infeld.com/en/home) fabrica encordoamentos dos mais diversos modelos para atender os diversos tipos de instrumentos musicais, sendo pioneira no desenvolvimento de encordoamentos de núcleo sintético para substituir o até então dominante mercado de cordas naturais, pode se encontrar mais informações sobre a história da empresa [aqui](https://www.thomastik-infeld.com/en/history)