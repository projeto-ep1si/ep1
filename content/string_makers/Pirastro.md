+++
title = 'Pirastro'
date = 2023-10-03T11:41:50-03:00
draft = false
+++
Empresa fundada em 1798 por Giorgio Pirazzi na Itália começou como uma fabricante de cordas familiar crescendo gradativamente, ganhando o nome conhecido hoje pela junção dos nomes dos sócios Pirazzi e Strobel, com a crescente demanda houve a expansão para a Alemanha, sendo uma empresa reconhecida mundialmente pela excelência das cordas fabricadas da maneira tradicional a mão. [Site oficial](https://www.pirastro.com/public_pirastro/pages/en/index.html)