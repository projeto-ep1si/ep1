Site voltado para orientar escolhas de encordoamentos para violinos e violas clássicos


Os principais tipos de cordas se dividem em encordoamentos com núcleo:

* **Orgânico**, este tipo possui por característica a fabricação em geral com tripas de carneiro e semelhantes, sendo a com o custo mais elevado dentre todos os encordoamentos, há a necessidade ainda de cuidados de manutenção das cordas para que nã haja uma redução precoce de suas características, sendo um tipo de encordoamento preferido entre musicistas profissionais, podemos citar seus principais modelos:

    * Pirastro [Eudoxa](https://www.pirastro.com/public_pirastro/pages/en/Eudoxa-00008/)

    * Pirastro [Oliv](https://www.pirastro.com/public_pirastro/pages/en/Oliv-00004/)

* **Sintético**; este tipo possui uma diferença na fabricação sendo feita com núcleo sintético, conseguindo assim um valor de mercado mais baixo se comparado com as naturais, sendo amplamente utilizadas por deter características de alto nível, sendo um tipo de encordoamento preferido entre musicistas de orquestra, podemos citar seus principais modelos:

    * Pirastro [Evah Pirazzi](https://www.pirastro.com/public_pirastro/pages/en/Evah-Pirazzi-00001/)

    * Pirastro [Obligato](https://www.pirastro.com/public_pirastro/pages/en/Obligato-00001/)

    * Pirastro [Tônica](https://www.pirastro.com/public_pirastro/pages/en/Tonica-00001/)

    * THOMASTIK [Dominant](https://www.thomastik-infeld.com/en/products/orchestral-strings/violin/dominant)

    * THOMASTIK [Rondo](https://www.thomastik-infeld.com/en/products/orchestral-strings/violin/rondo)


* **Aço**; sendo o modelo de encordoamento mais utilizado atualmente pois possui núcleo feito em aço, assim sendo encontrada com mais facilidade e com preço mais acessível, largamente utilizada por musicistas iniciantes e estudantes, por uma característica praticamente generalizada neste tipo de encordoamento que é a facilidade na resposta do mesmo, podemos citar seus principais modelos:

    * Pirastro [Chromcor](https://www.pirastro.com/public_pirastro/pages/en/Chromcor-00009/)

    * Pirastro [Piranito](https://www.pirastro.com/public_pirastro/pages/en/Piranito-00006/)
    
    * D'Addario [Preludio](https://www.lojadaddario.com.br/01203020006000000001/p)

 